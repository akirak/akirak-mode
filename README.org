* A collection of elisp functions
This is a library of Emacs Lisp functions which don't deserve individual packages.
Most of them have been extracted from my config for reuse across multiple configs.
They are quality-controlled to allow public use.
Feel free to use it.
